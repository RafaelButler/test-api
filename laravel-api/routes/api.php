<?php

use App\Http\Controllers\GenreMoviesController;
use App\Http\Controllers\SingleMovie;
use App\Http\Controllers\TopRated;
use App\Http\Controllers\UpComingMovies;
use Illuminate\Support\Facades\Route;

//Endpoint to get a list of genres
Route::get('/genres', [GenreMoviesController::class, 'getList']);

//Endpoint to get return a single genre by id
Route::get('/genre/{id}', [GenreMoviesController::class, 'getByID']);

//Endpoint to get a list of upcoming movies
Route::get('/movie/upcoming', [UpComingMovies::class, 'getList']);

//Endpoint to get a list of upcoming movies next 20 movies as page param is given
Route::get('/movie/upcoming/{page}', [UpComingMovies::class, 'getByPage']);

//Endpoint to get a list of top rated movies
Route::get('/movie/top_rated', [TopRated::class, 'getList']);

//Endpoint to get a list of top rated movies next 20 movies as page param is given
Route::get('/movie/top_rated/{page}', [TopRated::class, 'getByPage']);

//Endpoint to get a specific single movie
Route::get('/movie/{id}', [SingleMovie::class, 'getByID']);

//The same endpoint should return all movie related videos with a single request    
Route::get('/movie/{id}/videos', [SingleMovie::class, 'relatedVideos']);
