<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class UpComingMovies extends Controller
{
    public function __construct()
    {
        $this->url = env('API_ENV');
        $this->params = ['api_key' => env('API_KEY')];
    }

    public function getList()
    {
        try {
            // Route list TMDb
            $response = Http::get($this->url . '/movie/upcoming', $this->params);
            // Get content
            $movie_list = $response->json();

            return $movie_list;
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
    }

    public function getByPage($page)
    {
        try {
            // Route list TMDb passing params
            $response = Http::get($this->url . '/movie/upcoming', [
                'api_key' => env('API_KEY'),
                'page' => $page
            ]);
            // Get content
            $get_list_paginated = $response->json();
            return $get_list_paginated;
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
    }
}
