<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TopRated extends Controller
{
    public function __construct()
    {
        $this->url = env('API_ENV');
        $this->params = ['api_key' => env('API_KEY')];
    }

    public function getList()
    {
        try {
            // Route list TMDb 
            $response = Http::get($this->url . '/movie/top_rated', $this->params);
            // Get content
            $top_rated_movie_list = $response->json();

            return $top_rated_movie_list;
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
    }

    public function getByPage($page)
    {
        try {
            // Route list TMDb passing params
            $response = Http::get($this->url . '/movie/top_rated', [
                'api_key' => env('API_KEY'),
                'page' => $page
            ]);
            // Get content
            $top_rated_movie_paginated = $response->json();
            return $top_rated_movie_paginated;
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
    }
}
