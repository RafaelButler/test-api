<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SingleMovie extends Controller
{
    public function __construct()
    {
        $this->url = env('API_ENV');
        $this->params = ['api_key' => env('API_KEY')];
    }

    public function getByID($id)
    {
        try {
            // Route getByid passing requets params 
            $response = Http::get($this->url . '/movie' . '/' . $id, $this->params);
            // Get content
            $movie = $response->json();
            return $movie;
        } catch (Exception $e) {

            return response()->json(['error' => 'Movie doesnt exists']);
        }
    }

    public function relatedVideos($id)
    {
        try {
            // Route getByid passing requets params 
            $response = Http::get($this->url . '/movie' . '/' . $id . '/' . 'videos', $this->params);
            // Get content
            $movie_video_related = $response->json();
            return $movie_video_related;
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
    }
}
