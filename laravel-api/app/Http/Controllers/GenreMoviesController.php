<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class GenreMoviesController extends Controller
{
    public function __construct()
    {
        $this->url = env('API_ENV');
        $this->params = ['api_key' => env('API_KEY')];
    }

    public function getList()
    {
        try {
            // Route list TMDb
            $response = Http::get($this->url . '/genre/movie/list', $this->params);
            // Get content
            $movie_genres = $response->json();

            return $movie_genres;
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
    }

    public function getByID($id)
    {

        try {
            // Route list TMDb
            $response = Http::get($this->url . '/genre/movie/list', $this->params);

            $response = $response->json();
            $genres = $response['genres'];

            // Filter in array to find genre by request param
            foreach ($genres as $genre) {
                if ($genre['id'] === (int)$id) {
                    // If success found
                    return response()->json($genre);
                }
            }
            // If not found
            return response()->json(['error' => 'Not Found', 'status' => 404]);
        } catch (Exception $e) {
            return response()->json(['error' => $e]);
        }
    }
}
