
### Skills/experiences 

- [x] Git
- [x] React/Vue/Angular
- [ ] Agile
- [x] Clean Code
- [x] Linux
- [x] MySQL/PostgreSQL
- [x] PHP
- [ ] Python/Bash Script
- [x] Object Oriented
- [x] Frameworks (Zend Framework 2, Laravel, Silex or alike)
- [x] API
- [x] Unit Tests
- [x] Docker
- [ ] DevOps
- [x] Self-learn
- [ ] Cloud Computing (AWS)
- [x] Critical Thinking
