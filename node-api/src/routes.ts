import { Router } from "express";
import { GenreMovie } from "./controllers/GenreMoviesController";
import { SingleMovie } from "./controllers/SingleMovie";
import { TopRated } from "./controllers/TopRatedMoviesController";
import { UpComingMovies } from "./controllers/UpComingMoviesController";

const routes = Router();

routes.get('/movie/upcoming', new UpComingMovies().getList);
routes.get('/movie/upcoming/:page', new UpComingMovies().getByPage);
routes.get('/movie/top_rated', new TopRated().getList);
routes.get('/genres', new GenreMovie().getList);
routes.get('/genres/:genre_id', new GenreMovie().getByID);
routes.get('/movie/:movie_id', new SingleMovie().getById);
routes.get('/movie/:movie_id/videos', new SingleMovie().relatedVideos);

export {routes};