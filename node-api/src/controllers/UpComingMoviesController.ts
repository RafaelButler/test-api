import { Request, Response } from "express";
import { api } from "../../services/api";

export class UpComingMovies {
    async getList(request: Request, response: Response){
        
        try {
            // Route list TMDb
            const api_response = await api.get('/movie/upcoming');
            // Get content
            const movie_list = api_response.data;
            return response.json(
                movie_list
            );
        } catch (error) {
            return response.status(400).json({ error: error });
        }
    }

    async getByPage(request: Request, response: Response){
        const { page } = request.params;

        try {
            // Route list TMDb passing params
            const api_response = await api.get('/movie/upcoming', { 
                params: {
                    page
                }
            });
            // Get content
            const get_list_paginated = api_response.data;
            return response.json(
                get_list_paginated
            );
        } catch (error) {
            return response.status(400).json({ error: error });
        }

    }
}