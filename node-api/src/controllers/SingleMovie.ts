import { Request, Response } from "express";
import { api } from "../../services/api";


export class SingleMovie {
    async getById(request: Request, response: Response){
        const { movie_id } = request.params;
        try {
            // Route getByid passing requets params 
            const api_response = await api.get(`/movie/${movie_id}`);
            // Get content
            const movie = api_response.data;
            return response.json(
                movie
            );
        } catch (error) {
            return response.status(400).json({ error: 'Movie doesnt exists' });
        }
    }

    async relatedVideos(request: Request, response: Response){
        const { movie_id } = request.params;
        try {
            // Route getByid passing requets params 
            const api_response = await api.get(`/movie/${movie_id}/videos`);
            // Get content
            const movie_video_related = api_response.data;
            return response.json(
                movie_video_related
            );
        } catch (error) {
            return response.status(400).json({ error: error });
        }
    }
}