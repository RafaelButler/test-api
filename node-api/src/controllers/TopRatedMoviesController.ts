import { Request, Response } from "express";
import { api } from "../../services/api";

export class TopRated {
    async getList(request: Request, response: Response){
        try {
            // Route list TMDb
            const api_response = await api.get('/movie/top_rated');
            // Get content
            const top_rated_movie_list = api_response.data;
            return response.json(
                top_rated_movie_list
            );
        } catch (error) {
            return response.status(400).json({ error: error });
        }
    }

    async getByPage(request: Request, response: Response){
        const { page } = request.params;

        try {
            // Route list TMDb passing params
            const api_response = await api.get('/movie/top_rated', {
                params: {
                    page
                }
            });
            // Get content
            const top_rated_movie_paginated = api_response.data;
            return response.json(
                top_rated_movie_paginated
            );
        } catch (error) {
            return response.status(400).json({ error: error });
        }
    }
}