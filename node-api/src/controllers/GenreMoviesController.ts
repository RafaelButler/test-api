import { Request, Response } from "express";
import { api } from "../../services/api";


export class GenreMovie {
    async getList(request: Request, response: Response){
        try {
            // Route list TMDb
            const api_response = await api.get('/genre/movie/list');
            // Get content 
            const movie_genres = api_response.data;
            return response.json(
                movie_genres 
            );
        } catch (error) {
            return response.status(400).json({ error: error });
        }       
    }

    async getByID(request: Request, response: Response){
        const {genre_id} = request.params;

        try {
            // Rota de listagem TMDb
            const api_response = await api.get('/genre/movie/list');
            const movie_genres = api_response.data;
            
            // Filter in array to find genre by request param
            const genre_found = movie_genres.genres.filter((genre: { id: string; }) => genre.id == genre_id);

            // If not found
            if(genre_found.length == 0){
                return response.status(404).json({ error: 'Genre not found' });
            }

            // If succsess found
            return response.json(
                genre_found
            );
        } catch (error) {
            return response.status(400).json({ error: error });
        }

    }
}