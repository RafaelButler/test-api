import axios from "axios";

export const api =  axios.create({
    baseURL: "https://api.themoviedb.org/3",
});

api.interceptors.request.use(config => {
    config.params = {
        api_key: process.env.API_KEY,
        language: 'pt-BR',
        ...config.params
    }

    return config;
})
