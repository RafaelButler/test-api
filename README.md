# Test Challenger

## How to run

Clone this repo:

```console
    git clone https://gitlab.com/RafaelButler/test-api.git
```

Enter the project directory:

```console
    cd node-api
```

After that create a .env file like .env.example with API_KEY.

---

Install dependencies

```console
    yarn install
    or
    npm install
```

---

Run local server

```console
    yarn dev
    or
    npm dev
```

---

Linkedin: [Rafael Butler](https://www.linkedin.com/in/rafaelbutler)